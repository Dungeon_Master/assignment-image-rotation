

#ifndef UNTITLED4_BMP_WRITE_READ_H
#define UNTITLED4_BMP_WRITE_READ_H
#include <stdio.h>
#include "image_struct.h"
#include "bmp.h"
#include <stdlib.h>
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    INVALID_OPEN,
    NON_BI_RGB,
    NON_BITMAPINFOHEADER,
    READ_INVALID_SIZE_DATA


};



enum read_status from_bmp( FILE* in, struct image* img );

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR_FIRST,
    WRITE_ERROR_SECOND,
    WRITE_HEADER_ERROR,
    WRITE_INVALID_BITS
};

enum write_status to_bmp( FILE* out, struct image const* img );
struct bmp_header make_header(struct image const *img);
#endif //UNTITLED4_BMP_WRITE_READ_H
