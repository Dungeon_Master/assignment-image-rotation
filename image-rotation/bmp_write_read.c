
#include "bmp_write_read.h"
#include <stdio.h>
#include <malloc.h>
#include <stdbool.h>
#define BM 0x4d42
#include "bmp.h"
#include "image_struct.h"

static uint32_t padding(const uint64_t img_width) {
    return (img_width % 4);
}

static bool write_header( FILE* f, const struct bmp_header*const bmpHeader ) {
    return fwrite( bmpHeader, sizeof( struct bmp_header ), 1, f );
}


struct bmp_header make_header(struct image const *img){

    struct bmp_header bh={0};
    bh.bfType=BM;
    bh.bfileSize=sizeof(struct bmp_header) + sizeof(struct pixel)*img->width*img->height + img->height*padding(img->width);
    bh.bfReserved=0;
    bh.bOffBits= sizeof(struct bmp_header);
    bh.biSize=40;
    bh.biWidth=img->width;
    bh.biHeight=img->height;
    bh.biPlanes=1;
    bh.biBitCount=24;
    bh.biCompression=0;

    bh.bfileSize=bh.bfileSize-bh.bOffBits;
    bh.biXPelsPerMeter=0;
    bh.biYPelsPerMeter=0;
    bh.biClrUsed=0;
    bh.biClrImportant=0;
    return bh;

}

static bool verify_the_validity(struct bmp_header bmpHeader){
    if(bmpHeader.bfType!=BM||
        bmpHeader.biBitCount!=24||
            bmpHeader.biCompression!=0)
        return false;
    else
        return true;
}
enum read_status from_bmp( FILE* in, struct image* img){
    if(in==NULL)
        return INVALID_OPEN;
    struct bmp_header bmpHeader;
    const uint32_t Padding=padding(img->width);
    if(fread(&bmpHeader,sizeof(struct bmp_header),1,in)!=1)
        return READ_INVALID_SIZE_DATA;
    if(!verify_the_validity(bmpHeader))
        return READ_INVALID_HEADER;

    img->width=((bmpHeader.biWidth));
    img->height=((bmpHeader.biHeight));

    img->data=malloc(bmpHeader.biWidth*bmpHeader.biHeight*sizeof(struct pixel));
    size_t t=0;
    for(size_t i=0;i<bmpHeader.biHeight;i++){
        t=t+fread(&(img->data[i*img->width]),sizeof(struct pixel),img->width,in);
        fseek(in,Padding,SEEK_CUR);
    }
    if(t<bmpHeader.biHeight){
        return READ_INVALID_BITS;
    }



    return READ_OK;

}

 enum write_status to_bmp( FILE* const out,
                           const struct image* const image){
    struct bmp_header header=make_header(image);
   size_t count = 0;
    const uint64_t h = image->height;
    const uint64_t w = image->width;
    const uint32_t Padding=padding(w);
    if(!write_header(out, &header)) return WRITE_HEADER_ERROR;
    fseek(out, sizeof(struct bmp_header), SEEK_SET);

    const struct pixel* pixel =  image->data;
    const uint8_t s=0;

     for(size_t i = 0; i < h; i++){
         count =count+ fwrite( pixel, sizeof(struct pixel)*w, 1, out);
        if( fwrite( &s, Padding, 1, out)!=1){
            return WRITE_INVALID_BITS;}
        pixel += w;
     }

     if(count < h){
         return WRITE_INVALID_BITS;
     }

    return WRITE_OK;
}


