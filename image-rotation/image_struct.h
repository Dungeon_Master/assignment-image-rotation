#ifndef INC_2_BMP_STRACT_H
#define INC_2_BMP_STRACT_H

#include <stdio.h>
#include <inttypes.h>

struct pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};
#endif //INC_2_BMP_STRACT_H
