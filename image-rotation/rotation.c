#include "rotation.h"
#include "image_struct.h"
#include <stdlib.h>
struct image rotate(  struct image image){
    struct image rotate_image={.height=image.width,.width=image.height,
            .data=(struct pixel *)malloc(image.width*image.height*sizeof(struct pixel))};
    for(size_t i=0;i<image.height;i++){
        for (size_t j = 0; j <image.width ; ++j) {
            rotate_image.data[(image.height-1-i)+image.height*j]=
                    image.data[image.width*i+j];
        }
    }
    return rotate_image;

}
