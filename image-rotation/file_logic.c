//
// Created by Alexandro on 23.01.2021.
//

#include "file_logic.h"
#include <stdio.h>
#include "bmp_write_read.h"
enum status_close close (FILE** file){
    int close=fclose(*file);
    if (!close)
        return C_OK;
    return C_ERROR;
}

enum status_open open (FILE** file, const char* path, const char* mode_open){
    *file=fopen(path,mode_open);
    if(*file==NULL){
        return O_ERROR;
    }
    return O_OK;
}

static char* const read_status_array[]={
        [INVALID_OPEN]="NULL instead of path\n",
        [READ_INVALID_HEADER]="Invalid header of file\n",
        [READ_INVALID_SIGNATURE]="Invalid signature of file\n",
        [READ_INVALID_BITS]="Application supported only 24-bit file\n",
        [NON_BI_RGB]="Application dont supported compressed file\n",
        [NON_BITMAPINFOHEADER]="type is not BITMAPINFOHEADER\n",
        [READ_INVALID_SIZE_DATA]="Invalid pixel-data of file\n",
        [READ_OK]="The file has been successfully read\n",
};

enum read_status read_printer(enum read_status readStatus){
    fprintf(stderr,"%s", read_status_array[readStatus]);
    return readStatus;
}
static char* const write_status_array[]={
        [WRITE_OK]="The file has been successfully write\n",
        [WRITE_ERROR_FIRST]="Error while writing\n",
        [WRITE_ERROR_SECOND]="Error while writing\n",
        [WRITE_HEADER_ERROR]="Write header error\n",
        [WRITE_INVALID_BITS]="Write invalid bites\n",
};


enum write_status write_printer(enum write_status writeStatus){
    fprintf(stderr,"%s", write_status_array[writeStatus]);
    return writeStatus;
}
