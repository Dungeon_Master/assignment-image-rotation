

#ifndef IMAGE_ROTATION_FILE_LOGIC_H
#define IMAGE_ROTATION_FILE_LOGIC_H
#include <stdio.h>
#include "util.h"
#include "bmp_write_read.h"
enum status_open {
    O_OK = 0,
    O_ERROR
};
enum status_close {
    C_OK = 0,
    C_ERROR
};



enum read_status read_printer(enum read_status readStatus);
enum write_status write_printer(enum write_status writeStatus);




enum status_open open (FILE** file, const char* path, const char* mode_open);
enum status_close close (FILE **pFile);
#endif //IMAGE_ROTATION_FILE_LOGIC_H
