#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "bmp.h"
#include "util.h"
#include "rotation.h"
#include "file_logic.h"
#include "bmp_write_read.h"
void usage() {
    fprintf(stderr, "Usage: ./print_header BMP_FILE_NAME\n"); 
}


int main( int argc, char** argv ) {
    FILE *in=NULL;
    FILE *out=NULL;
    if (argc != 2) usage();
    if (argc < 2) err("Not enough arguments \n" );
    if (argc > 2) err("Too many arguments \n" );
    

    struct image image={0};

    enum status_open statusOpen=open(&in,argv[1],"rb");
    if(statusOpen!=O_OK)
        err("cant open this file");
    statusOpen=open(&out,"out.bmp","wb");
    if(statusOpen!=O_OK)
        err("cant open this file");

    enum read_status readStatus=from_bmp(in,&image);
   
   if(read_printer(readStatus))
       return readStatus;
    struct image rotate_image=rotate(image);
  

    enum write_status writeStatus=to_bmp(out,&rotate_image);
  
    if(write_printer(writeStatus))
        return writeStatus;

    enum status_close statusClose=close(&out);
    if(statusClose!=C_OK){
        err("cant close");
    }
    
    return 0;
}
